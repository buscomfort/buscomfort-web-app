const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const RouteSchema = new Schema({
    especificacao: {
        type: String,
    },
    descricao: {
        type: String,
        required: [true, "Descrição da rota é obrigatório."]
    },
    onibus : {
        type: String,
        ref: 'bus'
    }
});

const Route = mongoose.model("Route", RouteSchema);

module.exports = Route;