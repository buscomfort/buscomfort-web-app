const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const WaypointSchema = new Schema({
  latLng: {
    type: Array,
    required: [true, "Latitude e longitude do ponto são obrigatórios."]
  },
  ehGaragem: {
    type: String,
  },
  rota : {
    type: String,
    ref: 'route'
  }
});

const Waypoint = mongoose.model("Waypoint", WaypointSchema);

module.exports = Waypoint;