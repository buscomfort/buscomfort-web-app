const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ReadingSchema = new Schema({
  latLng: {
    type: Array
  },
  temperatura: {
    type: String
  },
  ruido: {
    type: String
  },
  comfortoRuido: {
    type: String
  },
  vibracao: {
    type: String
  },
  umidade: {
    type: String
  },
  onibus: {
    type: String,
    ref: "bus"
  }
});

const Reading = mongoose.model("Reading", ReadingSchema);

module.exports = Reading;
