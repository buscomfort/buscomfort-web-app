const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const OnibusSchema = new Schema({
  codigo: {
    type: String,
    required: [true, "Código é obrigatório."]
  },
  idModulo: {
    type: String,
    required: [true, "O ID do módulo é obrigatório."]
  },
});

const Onibus = mongoose.model("Bus", OnibusSchema);

module.exports = Onibus;