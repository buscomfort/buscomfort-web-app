module.exports = function(app) {
    var busController = require('../controllers/busController');
    app.route('/buses')
        .get(busController.getAll)
        .post(busController.create);
    app.route('/buses/:id')
        .get(busController.get)
        .put(busController.update)
        .delete(busController.remove);
};