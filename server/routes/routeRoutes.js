module.exports = function(app) {
    var routeController = require('../controllers/routeController');
    app.route('/routes')
        .get(routeController.getAll)
        .post(routeController.create);
    app.route('/routes/:id')
        .get(routeController.get)
        .put(routeController.update)
        .delete(routeController.remove);
};