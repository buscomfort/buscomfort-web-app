module.exports = function(app) {
    var readingController = require('../controllers/readingController');
    app.route('/readings')
        .get(readingController.getAll)
        .post(readingController.create);
    app.route('/readings/:id')
        .get(readingController.get)
        .put(readingController.update)
        .delete(readingController.remove);
};