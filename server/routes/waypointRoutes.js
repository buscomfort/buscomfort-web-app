module.exports = function(app) {
    var waypointController = require('../controllers/waypointController');
    app.route('/waypoints')
        .get(waypointController.getAll)
        .post(waypointController.create);
    app.route('/waypoints/:id')
        .get(waypointController.get)
        .put(waypointController.update)
        .delete(waypointController.remove);
};