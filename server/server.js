const express = require("express");
const path = require("path");
const cors = require("cors");
const mqtt = require("mqtt");
var public = path.join(__dirname, "public");

var readData = Buffer.from("");

const client = mqtt.connect("mqtt://192.168.0.181");

const port = null;

const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const Bus = require("./models/busModel");
const Reading = require("./models/readingModel");
const Waypoint = require("./models/waypointModel");
const Route = require("./models/routeModel");

// const parser = port.pipe(new Readline({ delimiter: '\n' }));
// Nosso pacote de dados do busModule recebido pelo LoRa tem 45 bytes no total
// 2 bytes de id + 41 bytes de dados + 2 bytes de CRC
// const parser = port.pipe(new ByteLength({length: 45}));

const app = express();

global.gpsData = null;

function nmeaConverter(value, direction) {
  /**
   * @params:
   * @value String com o valor de lat e lng gerado pelo gps
   */
  DD = parseInt(parseFloat(value) / 100);
  SS = parseFloat(value) - DD * 100;
  decimal = DD + SS / 60;
  return direction == "S" || direction == "W" ? -1 * decimal : decimal;
}

function distance(lat1, lon1, lat2, lon2) {
  R = 6371000;
  dLat = lat2 - lat1;
  dLon = lon2 - lon1;

  a =
    Math.pow(Math.sin(dLat / 2), 2) +
    Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dLon / 2), 2);
  c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  return R * c;
}

function getDateTime(date, time) {
  day = parseInt(date[0] + date[1]);
  month = parseInt(date[2] + date[3]);
  year = parseInt("20" + date[4] + date[5]);
  hour = parseInt(time[0] + time[1]);
  min = parseInt(time[2] + time[3]);
  sec = parseInt(time[4] + time[5]);
  return new Date(year, month - 1, day, hour, min, sec).toUTCString();
}

/**
 *
 * @param {String} value [knot]
 * @return {Float} value [km/h]
 */
function convertSpeed(value) {
  valueFloat = parseFloat(value);
  return valueFloat * 1.852;
}
var frameDict = {
  ID: 0, // Int16 LittleEndian from 0 to 2
  DAY: 2,
  MONTH: 3,
  YEAR: 4,
  HOURS: 5,
  MINUTES: 6,
  SECONDS: 7,
  STATUS: 8,
  LATITUDE: 9, // LittleEndian from 9 to 12
  LATITUDE_0: 9,
  LATITUDE_1: 10,
  LATITUDE_2: 11,
  LATITUDE_3: 12,
  LONGITUDE: 13, // LittleEndian from 13 to 16
  LONGITUDE_0: 13,
  LONGITUDE_1: 14,
  LONGITUDE_2: 15,
  LONGITUDE_3: 16,
  SPEED: 17, // LittleEndian from 17 to 20
  SPEED_0: 17,
  SPEED_1: 18,
  SPEED_2: 19,
  SPEED_3: 20,
  TEMPERATURE: 21,
  HUMIDITY: 22,
  MAXNOISEDB: 23, // LittleEndian from 23 to 26
  MAXNOISEDB_0: 23,
  MAXNOISEDB_1: 24,
  MAXNOISEDB_2: 25,
  MAXNOISEDB_3: 26,
  NOISECOUNT0: 27,
  NOISECOUNT0_0: 27,
  NOISECOUNT0_1: 28,
  NOISECOUNT1: 29,
  NOISECOUNT1_0: 29,
  NOISECOUNT1_1: 30,
  NOISECOUNT2: 31,
  NOISECOUNT2_0: 31,
  NOISECOUNT2_1: 32,
  NOISECOUNT3: 33,
  NOISECOUNT3_0: 33,
  NOISECOUNT3_1: 34,
  NOISECOUNT4: 35,
  NOISECOUNT4_0: 35,
  NOISECOUNT4_1: 36,
  JXCOUNT0: 37,
  JXCOUNT0_0: 37,
  JXCOUNT0_1: 38,
  JYCOUNT0: 39,
  JYCOUNT0_0: 39,
  JYCOUNT0_1: 40,
  JZCOUNT0: 41,
  JZCOUNT0_0: 41,
  JZCOUNT0_1: 42,
  CRC: 43 // Int16 LittleEndian from 43 to 44
};

function decodeFrameTrans(frameBuffer) {
  // Verificação do CRC da mensagem, caso seja inválido, então temos que limpar o buffer completamente
  crc = frameBuffer.readUInt16LE(frameDict.CRC);
  computed_crc = ComputeCRC(frameBuffer);
  console.log(frameBuffer);

  if (parseInt(crc) !== parseInt(computed_crc)) {
    if (port)
      port.flush(function (err, results) { });
    console.log("CRC ERROR, CRC: ", crc, "CRC_COMPUTED: ", computed_crc);
    return "CRC_ERROR";
  }

  id = frameBuffer.readUInt16LE(frameDict.ID);
  day = frameBuffer.readUInt8(frameDict.DAY);
  month = frameBuffer.readUInt8(frameDict.MONTH);
  year = frameBuffer.readUInt8(frameDict.YEAR);
  hours = frameBuffer.readUInt8(frameDict.HOURS);
  minutes = frameBuffer.readUInt8(frameDict.MINUTES);
  seconds = frameBuffer.readUInt8(frameDict.SECONDS);
  status = frameBuffer.readUInt8(frameDict.STATUS);
  latitude = frameBuffer.readFloatLE(frameDict.LATITUDE);
  longitude = frameBuffer.readFloatLE(frameDict.LONGITUDE);
  speed = frameBuffer.readFloatLE(frameDict.SPEED);
  temperature = frameBuffer.readUInt8(frameDict.TEMPERATURE);
  humidity = frameBuffer.readUInt8(frameDict.HUMIDITY);
  // noiseraw = frameBuffer.readFloatLE(frameDict.NOISERAW);
  maxnoisedb = frameBuffer.readFloatLE(frameDict.MAXNOISEDB);
  noisecount0 = frameBuffer.readUInt16LE(frameDict.NOISECOUNT0);
  noisecount1 = frameBuffer.readUInt16LE(frameDict.NOISECOUNT1);
  noisecount2 = frameBuffer.readUInt16LE(frameDict.NOISECOUNT2);
  noisecount3 = frameBuffer.readUInt16LE(frameDict.NOISECOUNT3);
  noisecount4 = frameBuffer.readUInt16LE(frameDict.NOISECOUNT4);
  jxcount0 = frameBuffer.readUInt16LE(frameDict.JXCOUNT0);
  jycount0 = frameBuffer.readUInt16LE(frameDict.JYCOUNT0);
  jzcount0 = frameBuffer.readUInt16LE(frameDict.JZCOUNT0);

  let noiseTotalReadings =
    noisecount0 + noisecount1 + noisecount2 + noisecount3 + noisecount4;
  noiseComfortIndex =
    (noisecount0 * 0.0 +
      noisecount1 * 0.25 +
      noisecount2 * 0.5 +
      noisecount3 * 0.75 +
      noisecount4) /
    noiseTotalReadings;

  year = 2000 + year;
  jsonNinjaDosParanaue = {
    state: 200,
    loraId: id,
    status: status,
    datetime: new Date(
      Date.UTC(year, month - 1, day, hours, minutes, seconds)
    ).toUTCString(),
    latitude: latitude,
    longitude: longitude,
    speed: speed,
    temperature: temperature,
    humidity: humidity,
    maxnoisedb: maxnoisedb,
    noisecount0: noisecount0,
    noisecount1: noisecount1,
    noisecount2: noisecount2,
    noisecount3: noisecount3,
    noisecount4: noisecount4,
    jxcount0: jxcount0,
    jycount0: jycount0,
    jzcount0: jzcount0,
    noiseComfortIndex: noiseComfortIndex
  };
  console.log(jsonNinjaDosParanaue);
  return jsonNinjaDosParanaue;
}
function ComputeCRC(frameBuffer) {
  i = 0;
  bitbang = 0;
  j = 0;
  crc_calc = 0xc181;

  for (i = 0; i < frameBuffer.byteLength - 2; i++) {
    crc_calc ^= frameBuffer.readUInt8(i) & 0x00ff;

    for (j = 0; j < 8; j++) {
      bitbang = crc_calc;
      crc_calc >>= 1;

      if (bitbang & 1) {
        crc_calc ^= 0xa001;
      }
    }
  }
  return crc_calc & 0xffff;
}

app.use(express.static(public));

mongoose.Promise = global.Promise;
mongoose.connect(
  "mongodb://admin:admin123@ds141178.mlab.com:41178/bus-comfort",
  { useNewUrlParser: true }
);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
const busRoutes = require("./routes/busRoutes");
const readingRoutes = require("./routes/readingRoutes");
const routeRoutes = require("./routes/routeRoutes");
const waypointRoutes = require("./routes/waypointRoutes");
busRoutes(app);
readingRoutes(app);
waypointRoutes(app);
routeRoutes(app);

app.get("/gps", (req, res) => {
  res.sendFile(path.join(public, "index.html"));
});

jsonNinjaDosParanaueFixo = {
  status: 200,
  datetime: "Mon, 30 Sep 2019 20:56:00 GMT",
  latitude: -9.4278921,
  longitude: -40.5712314,
  speed: 0.25372400879859924,
  temperature: 38,
  humidity: 87,
  maxnoisedb: 50.0,
  noisecount0: 0,
  noisecount1: 0,
  noisecount2: 0,
  noisecount3: 0,
  noisecount4: 0,
  jxcount0: 0,
  jycount0: 0,
  jzcount0: 0
};
// Descomentar a linha abaixo para ter dados fixos como resposta da API no markers[0]
// global.gpsData = jsonNinjaDosParanaueFixo;

app.post("/getData", (req, res) => {
  // Descomentar a linha abaixo para ter dados fixos como resposta da API no markers[0]
  // global.gpsData = jsonNinjaDosParanaueFixo;

  res.json(global.gpsData);
});

client.on("connect", function () {
  client.subscribe("bus/data", function (err) {
    console.log("trying");
    if (!err) {
      client.publish("bus/data", "Hello mqtt");
    }
  });
});

var request = require("request");
function saveReading(data) {
  let reading = {
    latLng: [data.latitude, data.longitude],
    temperatura: data.temperature,
    ruido: data.maxnoisedb,
    comfortoRuido: data.noiseComfortIndex,
    vibracao: data.jycount0,
    umidade: data.humidity,
    idModulo: data.loraId
  };
  var clientServerOptions = {
    uri: "http://localhost:3000/readings",
    body: JSON.stringify(reading),
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    }
  };
  request(clientServerOptions, function (error, response) {
    console.log(error, response.body);
    return;
  });
}

client.on("message", function (topic, message) {
  // message is Buffer
  console.log(message);
  if (message.byteLength == 45) {
    let dados = decodeFrameTrans(message);
    if (dados !== "CRC_ERROR") {
      let state = dados.status;
      saveReading(dados);
      if (state) {
        switch (state) {
          case 0x86:
            global.gpsData = {
              state: 404,
              msg: "Sinal não encontrado!"
              // datetime: getDateTime(dados[9], dados[1])
            };
            break;
          case 0x41:
            console.log(" DADOS");
            global.gpsData = dados;
            break;
          default:
            console.log(" DEFAUT");
            global.gpsData = {
              state: 404,
              msg: "Sinal não encontrado!"
              // datetime: getDateTime(dados[9], dados[1])
            };
            break;
        }
      }
    } else {
      console.log(" CRC ERROR");
    }
  }
});

if (port) {
  port.on("data", function (data) {
    readData = Buffer.concat([readData, data]);

    console.log(readData);
    if (readData.byteLength == 29) {
      let dados = decodeFrameTrans(readData);
      if (dados !== "CRC_ERROR") {
        let state = dados.status;
        if (state) {
          switch (state) {
            case 0x86:
              global.gpsData = {
                state: 404,
                msg: "Sinal não encontrado!"
                // datetime: getDateTime(dados[9], dados[1])
              };
              break;
            case 0x41:
              global.gpsData = dados;
              break;
            default:
              console.log(" DEFAUT");
              global.gpsData = {
                state: 404,
                msg: "Sinal não encontrado!"
                // datetime: getDateTime(dados[9], dados[1])
              };
              break;
          }
        }
      }
      readData = Buffer.from("");
    } else if (readData.byteLength > 29) {
      port.flush(function (err, results) { });
      readData = Buffer.from("");
    }
  });
};

//$GPRMC,150023.00,A,0924.79199,S,04030.97658,W,0.375,,040719,,,A*72
// parser.on('data', data =>{
// 	dados= decodeFrameTrans(data);

// });
let server_port = 3000;

app.listen(server_port, function () {
  console.log("Servidor rodando na porta " + server_port + "!");
});
