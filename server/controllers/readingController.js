const mongoose = require('mongoose');
const Reading = require('../models/readingModel');
const Bus = require('../models/busModel');

exports.getAll = function(req, res) {
    Reading.find(req.query, function(err, data){
        if (err)
            return res.status(500).send(err)
        return res.send(data);
    });
};

exports.create = function(req, res) {
    Bus.findOne({ idModulo: req.body.idModulo }, function(err, data){
        if (err)
            return res.status(500).send(err)
        req.body.onibus = data._id;
        let reading = new Reading(req.body);
        reading.save(function(reading_err, reading_res){
            if (reading_err)
                return res.status(500).send(reading_err)
            return res.json(reading_res);
        });
    });
};

exports.get = function(req, res) {
    Reading.find({onibus: req.params.id}).limit(30).exec( function(err, data){
        if (err)
            return res.status(500).send(err)
        return res.json(data);
    });
};

exports.update = function(req, res) {
    Reading.findOneAndUpdate(req.params.id, req.body, {new: true}, function(
    err, data){
        if (err)
            return res.status(500).send(err)
        return res.json(data);
    });
};
exports.remove = function(req, res) {
    Reading.findOneAndRemove(req.params.id, function(err, data){
        if (err)
            return res.status(500).send(err)
            return res.json({message: "Record successfully deleted."});
        });
};
