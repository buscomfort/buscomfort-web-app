const mongoose = require('mongoose');
const Route = require('../models/routeModel');

exports.getAll = function(req, res) {
    Route.find(req.query, function(err, data){
        if (err)
            return res.status(500).send(err)
        return res.send(data);
    });
};

exports.create = function(req, res) {
    let data = new Route(req.body);
    data.save(function(err, data){
        if (err)
            return res.status(500).send(err)
        return res.json(data);
    });
};

exports.get = function(req, res) {
    Route.findOne({ onibus: req.params.id }, function(err, data){
        if (err)
            return res.status(500).send(err)
        return res.json(data);
    });
};

exports.update = function(req, res) {
    Route.findOneAndUpdate(req.params.id, req.body, {new: true}, function(
    err, data){
        if (err)
            return res.status(500).send(err)
        return res.json(data);
    });
};
exports.remove = function(req, res) {
    Route.findOneAndRemove(req.params.id, function(err, data){
        if (err)
            return res.status(500).send(err)
            return res.json({message: "Record successfully deleted."});
        });
};
