const mongoose = require('mongoose');
const Bus = require('../models/busModel');
const Route = require('../models/routeModel');
const Waypoint = require('../models/waypointModel');

exports.getAll = function(req, res) {
    Bus.find(req.query, function(err, data){
        if (err)
            return res.status(500).send(err)
        return res.send(data);
    });
};

exports.create = function(req, res) {
    let data = new Bus(req.body);
    let route = new Route(req.body);
    let waypoint;
    data.save(function(err, data){
        if (err)
            return handleError(err);

        route.onibus = data._id;
        route.save(function(route_err, route){
            if (route_err)
                return handleError(route_err);
            
            for (let i in req.body.waypoints) {
                waypoint = new Waypoint(req.body.waypoints[i]);
                waypoint.rota = route._id;
                waypoint.save(function(waypoint_err){
                    if (waypoint_err)
                        return handleError(waypoint_err);
                });
            }
        });
        return res.json({
            'onibus': data,
            'rota': route,
            'waypoints': req.body.waypoints,
        });
    });
};

exports.get = function(req, res) {
    Bus.findById(req.params.id, function(err, data){
        if (err)
            return res.status(500).send(err)
        return res.json(data);
    });
};

exports.update = function(req, res) {
    delete req.body.id;
    let route_res = null;
    let waypoint;
    Bus.findOneAndUpdate(req.params.id, req.body, {new: true}, function(
    err, data){
        if (err)
            return res.status(500).send(err);
        
        Route.findOneAndUpdate(req.params.id, req.body, {new: true}, function(
        route_err, route){
            if (route_err)
                return handleError(route_err);

            route_res = route;
            Waypoint.deleteMany({ rota: route._id}, function(remove_err) {
                if (remove_err)
                    return handleError(remove_err);
            });
            for (let i in req.body.waypoints) {
                waypoint = new Waypoint(req.body.waypoints[i]);
                waypoint.rota = route._id;
                
                waypoint.save(function(waypoint_err){
                    if (waypoint_err)
                        return handleError(waypoint_err);
                });
            }
        });
        
        return res.json({
            'onibus': data,
            'rota': route_res,
            'waypoints': req.body.waypoints,
        });
    });
};
exports.remove = function(req, res) {
    Bus.findOneAndRemove(req.params.id, function(err, data){
        if (err)
            return res.status(500).send(err)
            return res.json({message: "Record successfully deleted."});
        });
};
