import Vue from 'vue'
import Router from 'vue-router'
import LiveMap from '@/features/live_map'
import About from '@/features/about'
import History from '@/features/history'
import Settings from '@/features/settings'
import AddBusPage from '@/features/settings/AddBusPage.vue'
import EditBusPage from '@/features/settings/EditBusPage.vue'

Vue.use(Router);

const routes = [
	{
		path: '/',
		name: 'LiveMap',
		component: LiveMap
	},
	{
		path: '/sobre',
		name: 'About',
		component: About
	},
	{
		path: '/historico/:id',
		name: 'History',
		component: History
	},
	{
		path: '/config',
		name: 'Settings',
		component: Settings
	},
	{
		path: '/config/adicionar-onibus',
		name: 'AddBusPage',
		component: AddBusPage
	},
	{
		path: '/config/editar-onibus/:id',
		name: 'EditBusPage',
		component: EditBusPage
	},
	{
		path: '*',
		beforeEnter (_, __, next) {
			next({ path: '/' });
		},
	}
];

const router = new Router({
	mode: 'history',
	routes,
});

export default router;
