import Api from './Api'

export default {
  fetchReadings () {
    return Api().get('readings')
  },

  addReading (params) {
    return Api().post('readings', params)
  },

  updateReading (params) {
    return Api().put('readings/' + params.id, params)
  },

  getReadingsbyBus (params) {
    return Api().get('readings/' + params.onibus_id)
  },

  deleteReading (id) {
    return Api().delete('readings/' + id)
  }
}