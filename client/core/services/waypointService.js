import Api from './Api'

export default {
  fetchWaypoints () {
    return Api().get('waypoints')
  },

  addWaypoint (params) {
    return Api().post('waypoints', params)
  },

  updateWaypoint (params) {
    return Api().put('waypoints/' + params.id, params)
  },

  getByRouteWaypoint (params) {
    return Api().get('waypoints/'+params.rota_id)
  },

  deleteWaypoint (id) {
    return Api().delete('waypoints/' + id)
  }
}