import Api from './Api'

export default {
  fetchBuses () {
    return Api().get('buses')
  },

  addBus (params) {
    return Api().post('buses', params)
  },

  updateBus (params) {
    return Api().put('buses/' + params.id, params)
  },

  getBus (params) {
    return Api().get('buses/' + params.id)
  },

  deleteBus (id) {
    return Api().delete('buses/' + id)
  }
}