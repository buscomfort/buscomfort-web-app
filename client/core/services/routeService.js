import Api from './Api'

export default {
  fetchRoutes () {
    return Api().get('routes')
  },

  addRoute (params) {
    return Api().post('routes', params)
  },

  updateRoute (params) {
    return Api().put('routes/' + params.id, params)
  },

  getByOnibusRoute (params) {
    return Api().get('routes/'+params.onibus_id)
  },

  getRoute (params) {
    return Api().get('routes/' + params.id)
  },

  deleteRoute (id) {
    return Api().delete('routes/' + id)
  }
}