# buscomfort-web-app

Aplicação web para o BusComfort feita com Node.js.

PARA FAZER A INSTALAÇÃO ABRA O TERMINAL NA PASTA ONDE ESTA O "PACKAGE.JSON" E DIGITE O COMANDO:

> npm install


## Requisitos

Esta aplicação possui as seguintes dependências

* [Express.js](https://expressjs.com/) >= 4.17.1
* [Node SerialPort](https://serialport.io/) >= 1.0.2
* [parser-byte-length](https://www.npmjs.com/package/@serialport/parser-byte-length) >= 7.1.5


